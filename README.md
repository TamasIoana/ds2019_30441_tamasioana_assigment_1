PROJECT SETUP (after clone)
1.Clone the project to your computer
2.Import the project in InteliJ
3.Create a database schema "medication" in MySQLWorkbench
4.Change the line from application.properties file located in /src/main/resources to 
allow table creation in the DB: spring.jpa.hibernate.ddl-auto = create
5.Run the application
6.Check if the tables were created 
7.Change the line from application.properties file located in /src/main/resources to 
allow table creation in the DB: spring.jpa.hibernate.ddl-auto = validate